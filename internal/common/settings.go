/*
*  Onur is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.

*  Onur is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.

*  You should have received a copy of the GNU General Public License
*  along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

package common

import (
	"fmt"
	"os"
	"path"

	"github.com/BurntSushi/toml"
)

type Settings struct {
	Git VCsettings `toml:"git"`
}

type VCsettings struct {
	SingleBranch bool `toml:"single-branch"`
	Quiet        bool `toml:"quiet"`
	Depth        int  `toml:"depth"`
}

func ReadSettings() Settings {
	settingsFile := path.Join(Configfolder(), "settings.toml")
	defaultSettings := Settings{Git: VCsettings{true, true, 1}}

	if _, err := os.Stat(settingsFile); err != nil {
		return defaultSettings
	}

	var settings Settings
	_, err := toml.DecodeFile(settingsFile, &settings)
	if err != nil {
		fmt.Fprint(os.Stderr, err)
		fmt.Println("")

		os.Exit(1)
	}

	return settings
}
