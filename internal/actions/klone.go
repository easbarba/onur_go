package actions

import (
	"fmt"
	"io"
	"os"
	"sync"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"

	"gitlab.com/easbarba/onur-go/internal/common"
	"gitlab.com/easbarba/onur-go/internal/domain"
)

// clone repository if none is found at folder
func Klone(dirpath string, project *domain.Project, settings *common.Settings, wg *sync.WaitGroup) {
	defer wg.Done()

	cloneOptions := &git.CloneOptions{
		URL:           project.URL,
		ReferenceName: plumbing.ReferenceName("refs/heads/" + project.Branch),
		SingleBranch:  settings.Git.SingleBranch,
		Depth:         settings.Git.Depth,
		Progress:      io.Discard,
		Tags:          git.NoTags,
	}
	_, err := git.PlainClone(dirpath, false, cloneOptions)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
