package actions

import (
	"fmt"
	"log"
	"os"
	"sync"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"

	"gitlab.com/easbarba/onur-go/internal/common"
	"gitlab.com/easbarba/onur-go/internal/domain"
)

// Pull repository at url/ and branch in the found folder
func Pull(dirpath string, project *domain.Project, settings *common.Settings, wg *sync.WaitGroup) {
	defer wg.Done()

	repo, err := git.PlainOpen(dirpath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	err = repo.Fetch(&git.FetchOptions{
		RemoteName: "origin",
		Depth:      settings.Git.Depth,
		Tags:       git.NoTags,
		Progress:   nil,
	})

	if err != nil && err != git.NoErrAlreadyUpToDate {
		log.Fatalf("Fetch failed: %v", err)
	}

	w, err := repo.Worktree()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	if err != nil {
		log.Fatalf("Failed to get worktree: %v", err)
	}

	commit, err := repo.ResolveRevision(plumbing.Revision("refs/remotes/origin/" + project.Branch))
	if err != nil {
		log.Fatalf("Failed to resolve revision: %v", err)
	}

	err = w.Reset(&git.ResetOptions{
		Commit: *commit,
		Mode:   git.HardReset,
	})
	if err != nil {
		log.Fatalf("Failed hard reseting repository: %v", err)
	}
}
