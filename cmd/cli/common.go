// Onur is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Onur is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Onur. If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"fmt"
)

func printProjectInfo(name, url, branch string) {
	message := fmt.Sprintf(`%-4s- %-35s %-75s  %s`, "",
		titleCrop(name, 30),
		titleCrop(url, 70),
		branch)
	fmt.Println(message)
}

func titleCrop(s string, length int) string {
	if len(s) > length {
		return s[:length] + "..."
	}

	return s
}
